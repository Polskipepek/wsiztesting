import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(rawUrl = GlobalVariable.urlh)

WebUI.delay(15)
if (WebUI.verifyElementPresent(findTestObject('Inne/maxTimer'), 5, FailureHandling.OPTIONAL)) {
	WebUI.comment('brak przycisku')
	WebUI.delay(5)
	WebUI.closeBrowser()
}else {
	WebUI.comment('Pomyslnie.')
}
WebUI.click(findTestObject('Inne/maxTimer'))

WebUI.delay(1)
if (WebUI.verifyElementPresent(findTestObject('Inne/maxTimert'), 5, FailureHandling.OPTIONAL)) {
	WebUI.comment('brak przycisku!')
	WebUI.delay(5)
	WebUI.closeBrowser()
}else {
	WebUI.comment('Pomyslnie.')
}
WebUI.click(findTestObject('Inne/maxTimer'))

WebUI.delay(1)
if (WebUI.verifyElementPresent(findTestObject('Inne/closeTimer'), 5, FailureHandling.OPTIONAL)) {
	WebUI.comment('brak przycisku!')
	WebUI.delay(5)
	WebUI.closeBrowser()
}else {
	WebUI.comment('Pomyslnie')
}
WebUI.click(findTestObject('Inne/closeTimer'))

WebUI.closeBrowser()

