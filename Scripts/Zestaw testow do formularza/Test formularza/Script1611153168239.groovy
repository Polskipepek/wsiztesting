import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//A proper use of a comment is to compensate for our failure to express ourselves in code.
//To the extend that we can express ourselves in code we do NOT need comments ~Uncle Bob

WebUI.openBrowser(rawUrl = GlobalVariable.urlh)
WebUI.click(findTestObject('Home/HERE'))

WebUI.delay(5)

WebUI.setText(findTestObject('Form1/Password'), GlobalVariable.password)

WebUI.setText(findTestObject('Form1/Mail'), GlobalVariable.mail)

WebUI.setText(findTestObject('Form1/Domain'), GlobalVariable.domain)

WebUI.click(findTestObject('Form1/other'))

WebUI.click(findTestObject('Form1/com'))

WebUI.click(findTestObject('Form1/Consent'))

String scroll =
'''
var div = document.querySelector("#app > div > div.modal > div > div > div > div > div.terms-and-conditions__text > div.terms-and-conditions__text-content > p:nth-child(39)");
div.scrollBottom = div.scrollHeight;
'''

WebUI.executeJavaScript(div, null)

WebUI.click(findTestObject('Form1/Next'))

WebUI.delay(3)

WebUI.click(findTestObject('Form2a/Unselect'))

WebUI.click(findTestObject('Form2a/Closets'))

WebUI.click(findTestObject('Form2a/Ponies'))

WebUI.click(findTestObject('Form2a/Cotton'))

WebUI.click(findTestObject('Form2a/Download'))

WebUI.click(findTestObject('Form2a/Next'))

if (WebUI.verifyElementPresent(findTestObject('Form2a/Next'), 5, FailureHandling.OPTIONAL)) {
	WebUI.comment('Wystąpiły błędy w formularzu 2!')
	WebUI.delay(5)
	WebUI.closeBrowser()
}else {
	WebUI.comment('Pomyslnie wypełniono wszystkie pola.')
}

WebUI.closeBrowser()